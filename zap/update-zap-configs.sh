#!/bin/bash

sed -i "s/ZAP_PASSWORD/$ZAP_PASSWORD/g" zap/zap-configs.yaml
sed -i "s/ZAP_USERNAME/$ZAP_USERNAME/g" zap/zap-configs.yaml
sed -i "s/ZAP_DOMAINNAME/$ZAP_DOMAINNAME/g" zap/zap-configs.yaml
sed -i "s/ZAP_REPORT_NAME/$ZAP_REPORT_NAME/g" zap/zap-configs.yaml
sed -i "s/ZAP_URL_LOGIN/$ZAP_URL_LOGIN/g" zap/zap-configs.yaml

ESCAPED_CI_PROJECT_DIR=$(sed -e 's/[&\\/]/\\&/g; s/$/\\/' -e '$s/\\$//' <<<"$CI_PROJECT_DIR")
sed -i "s/CI_PROJECT_DIR/$ESCAPED_CI_PROJECT_DIR/g" zap/zap-configs.yaml
