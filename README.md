# How to use it
## 1. Create a new branch for OWASP ZAP
```
Create a branch named owasp-scan
```
## 2. Declaring variables in Gitlab
```
Settings > CI/CD > Variables
| Key              | Value                               |
|------------------|-------------------------------------|
| ZAP_DOMAINNAME   | https:\/\/test.example.com          |
| ZAP_PASSWORD     | test                                |
| ZAP_USERNAME     | test                                |
| ZAP_REPORT_NAME  | report-name                         |
| ZAP_URL_LOGIN    | https:\/\/test.example.com/login    |
```
<b>NOTE:</b> 
 - please <b>UNCHECK</b> Protect variable (in Flags) if you run on an unprotected branch.
 - If your site  doesn't have a login page, we don't declare ZAP_URL_LOGIN
## 3. Add a remote access to the project would like to scan vulnerability
```
1. Open file .gitlab-ci.yml
2. Update lines:
    + Add new:
    include:
        - remote: 'https://gitlab.com/hdwebsoft-public/owasp-scanner/raw/main/.gitlab-ci.yml'
    
    + Update stages:
    stages:
        - build
        - deploy
        - scan-vulnerability
    
    + Add new stage:
    stage: scan-vulnerability
    only:
        - owasp-scan
```
## 4. Create a new pipeline to execute a scanning
```
1. Build > Pipelines > Run pipeline
2. Choose branch owasp-scan to scan
3. Click on the button Run pipeline to start it
4. A report PDF file will created after scanning is completely
```
